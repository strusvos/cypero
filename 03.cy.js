describe('Third Question', () => {
  it('passes', () => {
    cy.visit("https://example.cypress.io/commands/actions")
      .get('.action-email')
      .type('MyFakeMail@email.com')
      .should('have.value', 'MyFakeMail@email.com')
  })
})
