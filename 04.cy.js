
describe('Fourth Question', () => {
  it('passes', () => {
    cy.request('GET', 'https://jsonplaceholder.typicode.com/todos').then(
      (response) => {
        expect(response.body[0]).to.have.keys(['userId', 'id', 'title', 'completed']);
      }
    )
  })
})
