
describe('Fifth Question', () => {
  it('passes', () => {
    cy.request('POST', 'https://jsonplaceholder.typicode.com/todos', { title: 'foo', body: 'bar', userId: 1 }).then(
      (response) => {
        expect(response.status === 201).to.be.true;
      }
    )
  })
})
